package iterator;

import java.util.Iterator;
import java.util.LinkedList;

public class IteratorTest {

    /**
     * @param args
     */
    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<Integer>();
        list.add(1);
        list.add(2);

        Iterable<Integer> iterable = list;

        Iterator<Integer> iter = iterable.iterator();

        while (iter.hasNext()) {
            iter.next();
        }

        for (Integer i : iterable) {
            System.out.println(i);
        }
        // TODO Auto-generated method stub

    }

}
