package sockets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class ReadWriteTest implements Runnable {

    public void run() {
        try {
            ServerSocket server = new ServerSocket(12000);
            Socket client = server.accept();
            interact(client);
        } catch (Exception e) {
        }
    }

    private void interact(Socket client) throws IOException {
        InputStream in = client.getInputStream();
        OutputStream out = client.getOutputStream();

        byte[] inBuf;
        int contentLength = 501;
        ;
        inBuf = new byte[25];

        int bytesRead;
        int totalBytesRead = 0;

        while (totalBytesRead < contentLength) {
            bytesRead = in.read(inBuf, 0, inBuf.length);
            System.out.println("Server got " + new String(inBuf));
            totalBytesRead += bytesRead;
            System.out.println(bytesRead + " " + totalBytesRead);

        }
        InputStream fileStream = new FileInputStream(new File(
                "src/datastructures.BitUtils.java"));

        int totalBytesWritten = 0;
        while ((bytesRead = fileStream.read(inBuf, 0, inBuf.length)) != -1) {
            totalBytesWritten += bytesRead;
            out.write(inBuf, 0, bytesRead);
            System.out.println("server " + totalBytesWritten);
            out.flush();
        }

        fileStream.close();

        System.out.println("server " + totalBytesWritten);

    }

    public static void main(String[] args) throws UnknownHostException,
            IOException, InterruptedException {
        Thread server = new Thread(new ReadWriteTest());
        server.start();

        Socket socket = new Socket("localhost", 12000);
        InputStream in = socket.getInputStream();
        OutputStream out = socket.getOutputStream();

        out.write(getBytes());

        int bytesRead;
        int totalBytesRead = 0;
        byte[] inBuf = new byte[10];

        while ((bytesRead = in.read(inBuf)) != -1) {
            totalBytesRead += bytesRead;
            System.out.println("client: " + totalBytesRead);
        }

        System.out.println(totalBytesRead);

        Thread.sleep(1000);
    }

    private static byte[] getBytes() {
        byte[] b = new byte[501];
        for (int i = 0; i < b.length; i++) {
            b[i] = 'A';
        }

        b[b.length - 1] = '\n';

        return b;
    }
}
