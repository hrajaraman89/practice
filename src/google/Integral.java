package google;

import java.util.ArrayList;

public class Integral {

    ArrayList<Integer> computeIntegral(ArrayList<Integer> image, int width) {
        int length = image.size() / width;
        ArrayList<Integer> sum = new ArrayList<Integer>(image.size());

        for (int i = 0; i < image.size(); i++) {
            sum.add(0);
        }

        computeIntegral(image, width, sum, width - 1, length - 1);
        return sum;
    }

    void computeIntegral(ArrayList<Integer> image, int width,
            ArrayList<Integer> sum, int column, int row) {

        int oneDimensionalIndex = getOneDimensionalIndex(column, row, width);
        if (!isValidIndex(oneDimensionalIndex, image))
            return;
        if (getElementAt(column, row, width, sum) != 0) {
            return;
        }

        int element = getElementAt(column, row, width, image);

        computeIntegral(image, width, sum, column, row - 1);
        computeIntegral(image, width, sum, column - 1, row);
        computeIntegral(image, width, sum, column - 1, row - 1);

        Integer top = getElementAt(column, row - 1, width, sum);
        Integer left = getElementAt(column - 1, row, width, sum);
        Integer topLeft = getElementAt(column - 1, row - 1, width, sum);

        Integer computedSumForElement = top + left + element - topLeft;
        setElementAt(column, row, width, sum, computedSumForElement);

        System.out.printf("Top %d Left %d Top Left %d Element %d\n", top, left,
                topLeft, element);

        System.out.printf("f(%d)=%d\n\n", oneDimensionalIndex,
                computedSumForElement);
    }

    private void setElementAt(int column, int row, int width,
            ArrayList<Integer> arrayList, int element) {
        int index = getOneDimensionalIndex(column, row, width);
        arrayList.set(index, element);
    }

    private Integer getElementAt(int column, int row, int width,
            ArrayList<Integer> arrayList) {

        int index = getOneDimensionalIndex(column, row, width);
        if (isValidIndex(index, arrayList)) {
            return arrayList.get(index);
        } else
            return 0;
    }

    private int getOneDimensionalIndex(int column, int row, int width) {
        return (column >= 0 && row >= 0) ? row * width + column : -1;
    }

    boolean isValidIndex(int index, ArrayList<Integer> arrayList) {
        return index >= 0 && index < arrayList.size();
    }

    public Integral() {
        String input = "1,3,4,2,2,2,1,3,1,1,3,1";
        int width = 4;

        ArrayList<Integer> image = getIntegerList(input);
        System.out.println(image);

        ArrayList<Integer> result = computeIntegral(image, width);
        System.out.println(result);
    }

    public static void main(String[] args) {
        new Integral();
    }

    private static ArrayList<Integer> getIntegerList(String input) {
        String[] split = input.split(",");

        ArrayList<Integer> image = new ArrayList<Integer>(split.length);
        for (String s : split) {
            image.add(Integer.parseInt(s));
        }
        return image;
    }
}
