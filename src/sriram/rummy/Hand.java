package sriram.rummy;

import java.util.Arrays;

public class Hand {

    private Card[] cards = new Card[7];

    public Hand(String s) {

        String[] temp = s.split(", ");
        for (int x = 0; x < 7; x++) {
            cards[x] = new Card(temp[x]);
        }
    }

    public void printHand() {
        for (Card card : cards) {
            System.out.print(card + " ");
        }
    }

    public void sortHand() {
        Arrays.sort(cards);
    }

    public Card[] getHand() {
        return cards;
    }

}
