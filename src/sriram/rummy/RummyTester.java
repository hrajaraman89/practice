package sriram.rummy;

import java.util.ArrayList;

public class RummyTester {

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub

        String key = "7D, AD, AC, 7C, AS, AH, 7H";
        key = "1D, 2D, 3D, 5S, 5C, 5D, 5H";
        Hand hand = new Hand(key);

        // Group by suites then sort by FV
        hand.sortHand();

        // Two Groups
        ArrayList<Card> group1 = new ArrayList<Card>();
        ArrayList<Card> group2 = new ArrayList<Card>();

        Card[] temp = hand.getHand();

        // Add the first card in the list to group1
        group1.add(temp[0]);

        for (int x = 1; x < temp.length; x++) {
            // find all the cards with the same FV
            if (temp[0].getFaceValue().equals(temp[x].getFaceValue())
                    && group1.size() < 4) {
                // add it to group1
                group1.add(temp[x]);
            }
        }

        // if the size of group1 is too small to be a set
        if (group1.size() < 3) {
            for (int x = 1; x < temp.length; x++) {
                // find all the cards with the same suite within a range.
                if (temp[0].getSuite().equals(temp[x].getSuite())
                        && group1.size() < 4) {
                    // adds to group1
                    group1.add(temp[x]);
                }
            }
        }
        for (Card cards : temp) {
            // All cards that aren't in group1
            // is in group2
            if (!group1.contains(cards)) {
                group2.add(cards);
            }
        }

        // Print group1
        for (Card c : group1) {
            System.out.print(c + " ");
        }
        System.out.println();

        // print group2
        for (Card r : group2) {
            System.out.print(r + " ");
        }

    }
}
