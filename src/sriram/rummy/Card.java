package sriram.rummy;

public class Card implements Comparable<Card> {

    private String faceValue;
    private String suite;

    public Card(String s) {
        faceValue = s.substring(0, 1);
        suite = s.substring(1);
    }

    public String getFaceValue() {
        return faceValue;
    }

    public void setFaceValue(String faceValue) {
        this.faceValue = faceValue;
    }

    public String getSuite() {
        return suite;
    }

    public void setSuite(String suite) {
        this.suite = suite;
    }

    // return true if the param has a higher fv
    public boolean compareFaceValue(Card s) {
        String key = "123456789TJQK";
        return (key.indexOf(faceValue) < key.indexOf(s.faceValue));
    }

    public boolean isEqual(Card s) {
        return (s.faceValue.equals(faceValue) && s.suite.equals(suite));
    }

    public String toString() {
        return faceValue + suite;
    }

    @Override
    public int compareTo(Card c) {
        String key = "1H2H3H4H5H6H7H8H9HTHJHQHKH"
                + "1D2D3D4D5D6D7D8D9DTDJDQDKD" + "1C2C3C4C5C6C7C8C9CTCJCQCKC"
                + "1S2S3S4S5S6S7S8S9STSJSQSKS";

        return (key.indexOf(faceValue + suite) - key.indexOf(c.getFaceValue()
                + c.getSuite()));
    }

}
