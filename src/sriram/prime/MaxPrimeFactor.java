package sriram.prime;

public class MaxPrimeFactor {
    public static void main(String[] args) {
        long dividend = 91;
        long n = 2;

        while (n < dividend) {
            if (dividend % n == 0) {
                dividend = dividend / n;

            }
            n++;
        }

        System.out.println(dividend + " " + n);

    }
}
