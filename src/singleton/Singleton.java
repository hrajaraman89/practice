package singleton;

public class Singleton {
    private static Singleton instance;
    int i;

    private Singleton() {

    }

    public static synchronized Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }

        return instance;
    }

    /**
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        Singleton s = Singleton.getInstance();
        System.out.println(++s.i);

        System.out.println(++Singleton.getInstance().i);
    }

}
