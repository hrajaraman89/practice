package nio;

import java.net.URI;
import java.net.URISyntaxException;

public class NioTest {

    public static void main(String[] args) throws URISyntaxException {
        String url = "http://www.google.com/dir1/file.txt";
        String url2 = "dir/file.txt";

        URI uri = new URI(url);
        System.out.println(uri.getRawPath());

        System.out.println(new URI(url2).getRawPath());
    }

}
