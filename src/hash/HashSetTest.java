package hash;

import java.util.HashSet;

public class HashSetTest {

    /**
     * @param args
     */
    public static void main(String[] args) {
        HashSet<BoardLocation> set = new HashSet<BoardLocation>();

        BoardLocation l1, l2, l3, l4, l5;
        l1 = new BoardLocation(4, 5);
        l2 = new BoardLocation(4, 5);
        l3 = new BoardLocation(4, 5);
        l4 = new BoardLocation(3, 6);
        l5 = new BoardLocation(3, 6);

        set.add(l1);
        set.add(l2);
        set.add(l3);
        set.add(l4);
        set.add(l5);

        System.out.println(set.size());

    }
}
