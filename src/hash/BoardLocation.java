package hash;

public class BoardLocation {
    private int x, y;

    public BoardLocation(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o instanceof BoardLocation) {

            BoardLocation that = (BoardLocation) o;

            return this.x == that.x && this.y == that.y;

        }

        return false;
    }

    public int hashCode() {
        return x + y;
    }
}
