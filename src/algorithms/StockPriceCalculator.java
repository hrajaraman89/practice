package algorithms;

import java.util.Arrays;
import java.util.Random;

public class StockPriceCalculator {

    public void getBestTrade_Linear(Trade[] trades) {
        long start, end;
        start = System.currentTimeMillis();
        int buyIndex, sellIndex;
        int length = trades.length;

        int minIndex = trades[0].price < trades[1].price ? 0 : 1;
        buyIndex = 0;
        sellIndex = 1;

        for (int i = 2; i < length; i++) {
            if (Trade.getProfit(trades[minIndex], trades[i]) > Trade.getProfit(
                    trades[sellIndex], trades[buyIndex])) {
                buyIndex = minIndex;
                sellIndex = i;
            }

            if (trades[i].price < trades[minIndex].price) {
                minIndex = i;
            }
        }

        end = System.currentTimeMillis();
        /*
         * System.out .printf(
         * "Buy on %d for $%d and sell on %d for $%d for total profit of $%d\n",
         * buyIndex, trades[buyIndex].price, sellIndex, trades[sellIndex].price,
         * Trade.getProfit(trades[buyIndex], trades[sellIndex]));
         */

        System.out.println("Linear " + (end - start));

    }

    public void getBestTrade_Sort(Trade[] trades) {
        long start, end;
        start = System.currentTimeMillis();
        Arrays.sort(trades);
        end = System.currentTimeMillis();

        System.out.println("Sort " + (end - start));

        // for (int highPrice = trades.length - 1; highPrice >= 0; --highPrice)
        // {
        // for (int lowPrice = 0; lowPrice < highPrice; lowPrice++) {
        //
        // }
        // }
    }

    public static void main(String[] args) {
        StockPriceCalculator calc = new StockPriceCalculator();

        final int numTrades = 1000000;
        Trade[] trades = generateTrades(numTrades);

        calc.getBestTrade_Linear(trades);
        calc.getBestTrade_Sort(trades);
    }

    private static Trade[] generateTrades(int numTrades) {
        Trade[] trades = new Trade[numTrades];

        Random random = new Random(System.currentTimeMillis());
        final int maxInt = 1000;
        final int offset = 5;

        for (int i = 0; i < trades.length; i++) {
            trades[i] = new Trade(random.nextInt(maxInt) + offset, i);
        }

        return trades;
    }
}

class Trade implements Comparable<Trade> {
    int price;
    private int date;

    Trade(int price, int date) {
        this.price = price;
        this.date = date;
    }

    public static int getProfit(Trade buyTrade, Trade sellTrade) {
        if (buyTrade.date >= sellTrade.date) {
            return Integer.MIN_VALUE;
        } else {
            return sellTrade.price - buyTrade.price;
        }
    }

    @Override
    public int compareTo(Trade arg0) {
        return this.price - arg0.price;
    }
}
