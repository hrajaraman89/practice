package algorithms;

public class DynamicPrograms {

    public static void maximizeProcessing(int[] capacity, int[] lag) {
        int[] opt = new int[capacity.length];

        for (int i = 0; i < opt.length; i++) {
            opt[i] = findOpt(i, capacity, lag, opt);
        }

        print(opt);
    }

    private static int findOpt(int i, int[] capacity, int[] lag, int[] opt) {
        int maxOpt = Math.min(capacity[i], lag[i]);

        for (int j = 1; j <= i; j++) {

            int currentOpt = Math.min(capacity[i], lag[i - j]) + opt[i - j];
            System.out.printf("i = %d, i-j = %d, currentOpt = %d\n", i, i - j,
                    currentOpt);

            if (currentOpt > maxOpt)
                maxOpt = currentOpt;
        }

        return maxOpt;
    }

    private static void print(int[] opt) {
        for (int i = 0; i < opt.length; i++) {
            System.out.print(opt[i] + "\t");
        }

        System.out.println();

    }

    private static void findOptimalMachine(int[] a, int[] b) {

        int[] optA = new int[a.length];
        int[] optB = new int[a.length];

        optA[0] = a[0];
        optB[0] = b[0];

        for (int i = a.length - 1; i >= 0; i--) {
            findOptMachineA(a, b, optA, optB, i);
            findOptMachineB(a, b, optA, optB, i);
        }

        System.out.println(optA[a.length - 1] + " " + optB[a.length - 1]);

    }

    private static void findOptMachineB(int[] a, int[] b, int[] optA,
            int[] optB, int i) {

        if (i <= 1 || optB[i] > 0)
            return;

        int option1 = 0, option2 = 0;

        if (i - 2 >= 0)
            findOptMachineA(a, b, optA, optB, i - 2);

        if (i - 1 >= 0)
            findOptMachineB(a, b, optA, optB, i - 1);

        if (i - 2 >= 0)
            option1 = optA[i - 2];
        if (i - 1 >= 0)
            option2 = optB[i - 1];

        optB[i] = b[i] + Math.max(option1, option2);

    }

    private static void findOptMachineA(int[] a, int[] b, int[] optA,
            int[] optB, int i) {

        if (optA[i] > 0)
            return;

        int option1 = 0, option2 = 0;

        if (i - 2 >= 0)
            findOptMachineB(a, b, optA, optB, i - 2);

        if (i - 1 >= 0)
            findOptMachineA(a, b, optA, optB, i - 1);

        if (i - 2 >= 0)
            option1 = optB[i - 2];

        if (i - 1 >= 0)
            option2 = optA[i - 1];

        optA[i] = a[i] + Math.max(option1, option2);

    }

    private static void findMinShipmentCost(int r, int c, int[] s) {
        int[] costs = new int[s.length];

        findCost(r, c, s, costs, s.length - 1);

        System.out.println(costs[s.length - 1]);
    }

    private static void findCost(int r, int c, int[] s, int[] costs, int i) {
        if (i >= s.length || i < 0)
            return;

        if (costs[i] > 0)
            return;

        findCost(r, c, s, costs, i - 4);
        findCost(r, c, s, costs, i - 1);

        int cost1 = 4 * c + ((i - 4 >= 0) ? costs[i - 4] : 0);
        int cost2 = r * s[i] + ((i - 1 >= 0) ? costs[i - 1] : 0);

        costs[i] = Math.min(cost1, cost2);
    }

    private static void findServers(int[] costs) {
        int[] servers = new int[costs.length];

        servers[servers.length - 1] = 0;
        for (int i = servers.length - 2; i >= 0; i--) {
            servers[i] = Math.min(costs[i], (servers[i + 1] + 1));

            System.out.printf("%sutting it on server %d\n",
                    (servers[i] == servers[i + 1] + 1) ? "Not p" : "P", i);
        }

        print(servers);
    }

    public static void main(String[] args) {
        // int[] capacity = new int[] { 10, 1, 7, 7 };
        // int[] lag = new int[] { 8, 4, 2, 1 };
        //
        // maximizeProcessing(capacity, lag);

        // int[] a = new int[] { 20, 30, 0, 80 };
        // int[] b = new int[] { 10, 10, 60, 10 };
        //
        // findOptimalMachine(a, b);

        // int r = 1, c = 10;
        // int[] s = new int[] { 11, 9, 9, 12, 12, 12, 12, 9, 9, 11 };

        // findMinShipmentCost(r, c, s);

        int[] costs = new int[] { 1, 2, 4, 8, 7, 6, 3, 10 };
        findServers(costs);

    }
}
