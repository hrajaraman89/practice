package algorithms;

public class SubSequence {

    public static void canUnravel(String x, String y, String sequence) {
        boolean[] opt = new boolean[sequence.length()];

        canUnravel(x, y, sequence, 0, 0, 0, opt);

        System.out.println(opt[0]);
    }

    private static void canUnravel(String x, String y, String sequence, int i,
            int j, int k, boolean[] opt) {

        if (i >= sequence.length())
            return;

        if (sequence.charAt(i) == x.charAt(j)) {
            System.out.printf(
                    "Sequence at index %d matches with %s at index %d\n", i, x,
                    j);
            canUnravel(x, y, sequence, i + 1, (j + 1) % x.length(), k, opt);
        } else if (sequence.charAt(i) == y.charAt(k)) {
            System.out.printf(
                    "Sequence at index %d matches with %s at index %d\n", i, y,
                    k);
            canUnravel(x, y, sequence, i + 1, j, (k + 1) % y.length(), opt);
        }

        if (i < opt.length - 1)
            opt[i] = opt[i + 1];
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        String sequence = "1001101";
        String x = "101";
        String y = "00";

        canUnravel(x, y, sequence);
    }

}
