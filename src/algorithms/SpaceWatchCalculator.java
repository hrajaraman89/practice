package algorithms;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class SpaceWatchCalculator {

    public static void outputEvents(int[] coordinates) {
        int[] opt = new int[coordinates.length];
        int[] previousEvent = new int[coordinates.length];

        for (int i = 0; i < opt.length; i++) {
            opt[i] = previousEvent[i] = Integer.MIN_VALUE;
        }
        getOpt(opt, opt.length - 1, coordinates, previousEvent);

        for (int i = 0; i < opt.length; i++)
            System.out.println(i + "\t" + opt[i] + "\t" + previousEvent[i]);

    }

    private static void getOpt(int[] opt, int i, int[] coordinates,
            int[] previousEvent) {

        if (opt[i] > 0)
            return;

        Queue<Integer> feasible = getFeasibleEvents(coordinates, i);
        Iterator<Integer> iterator = feasible.iterator();

        int max = 0;

        while (iterator.hasNext()) {
            int nextIndex = iterator.next();
            getOpt(opt, nextIndex, coordinates, previousEvent);
            if (opt[nextIndex] > max) {
                max = opt[nextIndex];
                previousEvent[i] = nextIndex;
            }
        }
        opt[i] = 1 + max;
    }

    private static Queue<Integer> getFeasibleEvents(int[] coordinates, int i) {
        Queue<Integer> feasible = new LinkedList<Integer>();

        for (int j = i - 1; j >= 0; j--) {
            if ((i - j) >= Math.abs(coordinates[i] - coordinates[j])) {
                feasible.add(j);
            }
        }

        return feasible;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        int[] coordinates = new int[] { 3, 2, -3, -2, -1, 0 };
        outputEvents(coordinates);
    }

}
