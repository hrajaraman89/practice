package algorithms;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class RisingTrend {

    public static void getRisingTrend(int[] prices) {
        int[] opt = new int[prices.length];
        opt[0] = 1;

        int[] previousIndex = new int[prices.length];

        for (int i = 0; i < previousIndex.length; i++) {
            previousIndex[i] = -1;
        }

        getOpt(opt, opt.length - 1, prices, previousIndex);

        for (int i = 0; i < prices.length; i++) {
            System.out.println(i + "\t" + opt[i] + "\t" + previousIndex[i]);
        }

    }

    private static void getOpt(int[] opt, int i, int[] prices,
            int[] previousIndex) {
        if (i == 0)
            return;

        Queue<Integer> lowerPricesIndices = getLowerPriceBefore(i, prices);

        Iterator<Integer> iter = lowerPricesIndices.iterator();

        int max = Integer.MIN_VALUE;

        while (iter.hasNext()) {
            int index = iter.next();
            getOpt(opt, index, prices, previousIndex);
            if (opt[index] > max) {
                max = opt[index];
                previousIndex[i] = index;
            }
        }

        opt[i] = max + 1;
    }

    private static Queue<Integer> getLowerPriceBefore(int i, int[] prices) {
        Queue<Integer> queue = new LinkedList<Integer>();

        for (int j = i - 1; j >= 0; j--) {
            if (prices[j] < prices[i]) {
                queue.add(j);
            }
        }

        return queue;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        int[] prices = new int[] { 1, 4, 2, 3 };

        getRisingTrend(prices);

    }

}
