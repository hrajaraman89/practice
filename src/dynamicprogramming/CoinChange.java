package dynamicprogramming;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Hari
 * Date: 8/20/13
 * Time: 7:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class CoinChange {

    public static List<Integer> getMinCoins(int[] coins, int target) {

        List<Integer> result = new ArrayList<Integer>();

        /**
         * c(t) = min(c(t-vi)) + 1
         */

        for (int i = 0; i < coins.length; i++) {
            if (target == coins[i]) {
                result.add(target);
                return result;
            }
        }

        int minNumCoins = Integer.MAX_VALUE;
        List<Integer> change = null;

        for (int i = 0; i < coins.length; i++) {
            int coinValue = coins[i];

            int newTarget = target - coinValue;

            if (newTarget > 0) {

                List<Integer> numCoinsNeeded = getMinCoins(coins, target - coinValue);

                int numCoins = numCoinsNeeded.size();

                if (numCoins < minNumCoins) {
                    change = numCoinsNeeded;
                    change.add(coinValue);
                    minNumCoins = numCoins;
                }
            }
        }

        return change;

    }

    public static void main(String[] args) {
        System.out.println(CoinChange.getMinCoins(new int[]{
                1, 2, 3, 5
        }, 8));
        System.out.println(CoinChange.getMinCoins(new int[]{
                1, 2, 5
        }, 9));

        System.out.println(CoinChange.getMinCoins(new int[]{
                1, 2, 3, 5
        }, 19));
    }
}
