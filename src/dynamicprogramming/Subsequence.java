package dynamicprogramming;

/**
 * Created with IntelliJ IDEA.
 * User: Hari
 * Date: 8/20/13
 * Time: 10:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class Subsequence {

    public static int longestSubsequence(int[] nums) {
        int[] opt = new int[nums.length];

        opt[0] = 1;

        for (int i = nums.length - 1; i > 0; i--) {
            setLongestSubSequenceAt(i, nums, opt);
        }

        int count = Integer.MIN_VALUE;

        for (int i = 0; i < opt.length; i++) {
         //   System.out.println(opt[i]);
            count = Math.max(count, opt[i]);
        }

        return count;
    }

    private static void setLongestSubSequenceAt(int i, int[] nums, int[] opt) {
        if (opt[i] > 0) {
            return;
        } else {
            opt[i] = -1;

            for (int j = i - 1; j >= 0; j--) {

                if (nums[i] > nums[j]) {
                    setLongestSubSequenceAt(j, nums, opt);
              //      System.out.printf("Long sequence for j=%d is %d\n", j, opt[j]);

                    opt[i] = Math.max(opt[i], opt[j] + 1);
                }
            }

         //   System.out.printf("Long sequence at %d is %d\n", i, opt[i]);
        }
    }

    public static void main(String[] args) {
        System.out.print(longestSubsequence(new int[]{0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15}));
    }
}
