package inheritance;

public class Superclass {

    public void A() {
        System.out.println("Super.A");
        this.B();
    }

    public void A(int x) {
        System.out.println("Super ax");
    }

    public void B() {
        System.out.println("Super B");
    }

    public static void main(String[] args) {
        Superclass sup = new Superclass();
        Subclass sub = new Subclass();

        sub.A();

        String x = "1b";

        System.out.println(Integer.parseInt(x, 16));
    }

}
