package inheritance;

public class Subclass extends Superclass {

    public void B() {
        System.out.println("Sub B");
        super.A(1);
    }
}
