package palantir;

/**
 * Created with IntelliJ IDEA.
 * User: Hari
 * Date: 8/22/13
 * Time: 2:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class FindAllRepeating {

    private static void printRepeating(int arr[]) {
        int i;
        int size = arr.length;
        System.out.println("The repeating elements are: \n");
        for (i = 0; i < size; i++) {
            int index = Math.abs(arr[i]);
            if (arr[index] >= 0) {
                arr[index] = -arr[index];
            } else
                System.out.printf(" %d ", index);
        }
    }

    public static void main(String[] args) {
        printRepeating(new int[]{1, 2, 3, 1, 3, 6, 6});
    }

    /**
     * i 0 index 1.. new array -1, 2, 3, 1, 3, 6, 6, 38
     * i 1 index 2.. new array -1, 2, -3,
     */
}
