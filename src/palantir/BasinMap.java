package palantir;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Scanner;

public class BasinMap {

    private Location[][] locations;
    private int numRows;
    private char nextLabel;
    private Hashtable<String, Integer> basinPartition;

    public BasinMap(int numRows, int[][] altitudes) {
        this.locations = new Location[numRows][numRows];
        this.numRows = numRows;
        this.basinPartition = new Hashtable<String, Integer>();

        this.nextLabel = 'A';

        fillLocations(numRows, altitudes);

    }

    private void fillLocations(int numRows, int[][] altitudes) {
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < altitudes.length; j++) {
                locations[i][j] = new Location(altitudes[i][j]);
            }
        }
    }

    private void calculate() {
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numRows; j++) {
                startFlowAtIndex(i, j);
            }
        }

        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numRows; j++) {
                setBasinLabel(i, j);
            }
        }

        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numRows; j++) {
                String label = locations[i][j].basinLabel;

                if (basinPartition.containsKey(label)) {
                    basinPartition.put(label, basinPartition.get(label) + 1);
                } else {
                    basinPartition.put(label, 1);
                }
            }
        }
    }

    private void setBasinLabel(int i, int j) {
        if (locations[i][j].isSink || locations[i][j].basinLabel != null) {
            return;
        } else {
            Location location = locations[i][j];
            Location flowLocation = locations[location.flowRow][location.flowColumn];
            if (flowLocation.basinLabel == null) {
                setBasinLabel(location.flowRow, location.flowColumn);
            }
            location.basinLabel = flowLocation.basinLabel;
        }
    }

    private void startFlowAtIndex(int row, int column) {
        ArrayList<Integer> adjacentLocations = findAdjacentLocations(row,
                column);

        int count = adjacentLocations.size();
        int minAltitude = Integer.MAX_VALUE;

        int currentRow, currentColumn, minRow = 0, minColumn = 0;

        for (int i = 0; i < count; i++) {
            int index = adjacentLocations.get(i);
            currentRow = index / numRows;
            currentColumn = index % numRows;

            if (locations[currentRow][currentColumn].altitude < minAltitude) {
                minAltitude = locations[currentRow][currentColumn].altitude;
                minRow = currentRow;
                minColumn = currentColumn;
            }
        }

        if (locations[row][column].altitude < minAltitude) {
            locations[row][column].isSink = true;
            locations[row][column].basinLabel = getNextLabel();
        } else {
            locations[row][column].flowRow = minRow;
            locations[row][column].flowColumn = minColumn;
        }
    }

    private String getNextLabel() {

        String ret = this.nextLabel + "";
        nextLabel++;
        return ret;
    }

    private ArrayList<Integer> findAdjacentLocations(int row, int column) {
        ArrayList<Integer> adjacentLocations = new ArrayList<Integer>();

        // top
        if (inBounds(row - 1)) {
            adjacentLocations.add(getSingleIndex(row - 1, column));
        }

        // left
        if (inBounds(column - 1)) {
            adjacentLocations.add(getSingleIndex(row, column - 1));
        }
        // right
        if (inBounds(column + 1)) {
            adjacentLocations.add(getSingleIndex(row, column + 1));
        }
        // bottom
        if (inBounds(row + 1)) {
            adjacentLocations.add(getSingleIndex(row + 1, column));
        }

        return adjacentLocations;

    }

    private Integer getSingleIndex(int row, int column) {
        return row * numRows + column;
    }

    private boolean inBounds(int index) {
        return index >= 0 && index < numRows;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        int numRows = reader.nextInt();

        int[][] altitudes = new int[numRows][numRows];

        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numRows; j++) {
                altitudes[i][j] = reader.nextInt();
            }
        }

        BasinMap map = new BasinMap(numRows, altitudes);

        map.calculate();
        map.printPartitionSizes();
        map.print();

    }

    private void printPartitionSizes() {
        Integer[] values = this.basinPartition.values().toArray(new Integer[0]);
        Arrays.sort(values);

        for (int i = values.length - 1; i >= 0; i--) {
            System.out.print(values[i] + " ");
        }
        System.out.println();
    }

    private void print() {
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numRows; j++) {
                System.out.print(locations[i][j].basinLabel + ' ');
            }
            System.out.println();
        }
    }

}

class Location {
    public int flowRow;
    public int flowColumn;
    int altitude;
    boolean isSink;
    String basinLabel;

    Location(int altitude) {
        this.altitude = altitude;
        flowRow = -1;
        flowColumn = -1;
    }
}
