package threading;

public class Worker implements Runnable {

    private WorkQueue queue;

    public Worker(WorkQueue queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while (true) {
            String work;
            try {
                work = queue.remove();
                if (work == null)
                    break;
                System.out.printf("%s doing work %s\n", Thread.currentThread()
                        .getName(), work);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
