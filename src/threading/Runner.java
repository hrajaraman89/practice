package threading;

public class Runner {

    public static void main(String[] args) {
        new Runner();
    }

    public Runner() {
        WorkQueue queue = new WorkQueue();
        ThreadPool pool = new ThreadPool(5, queue);

        createWork(queue, pool);
    }

    private void createWork(WorkQueue queue, ThreadPool pool) {
        int numWork = 100;

        for (int i = 0; i < numWork; i++) {
            queue.add(i + "");
            try {
                if (i % 5 == 0) {
                    pool.status();
                }
                Thread.sleep(50);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        queue.finish();

    }
}
