package threading;

public class ThreadPool {

    private Thread[] pool;

    public ThreadPool(int numThreads, WorkQueue queue) {
        pool = new Thread[numThreads];

        for (int i = 0; i < numThreads; i++) {
            pool[i] = new Thread(new Worker(queue));
            pool[i].setName(i + "");
            pool[i].start();
        }
    }

    void status() {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < pool.length; i++)
            builder.append(pool[i].getName() + ": "
                    + pool[i].getState().toString() + "\t");

        System.out.println(builder.toString());
    }
}
