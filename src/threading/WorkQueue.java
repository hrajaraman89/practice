package threading;

import java.util.LinkedList;

public class WorkQueue {

    Object lockObject;
    LinkedList<String> queue;
    private boolean workloadFinished;

    public WorkQueue() {
        lockObject = new Object();
        queue = new LinkedList<String>();
    }

    public void add(String work) {
        synchronized (lockObject) {
            queue.add(work);
            lockObject.notify();
        }
    }

    public String remove() throws InterruptedException {
        synchronized (lockObject) {
            while (queue.isEmpty() && !workloadFinished) {
                lockObject.wait();
            }
            if (!queue.isEmpty())
                return queue.remove();
            else
                return null;
        }
    }

    public void finish() {
        this.workloadFinished = true;
        synchronized (lockObject) {
            lockObject.notifyAll();
        }
    }

}
