package threading.wait;

import java.io.BufferedReader;
import java.io.FileReader;

public class Runner implements Runnable {

    public static void main(String[] args) {
        new Runner();
    }

    private Object lockObject = new Object();

    public Runner() {

        Thread t1 = new Thread(this, 1000 + "");
        t1.start();
        try {
            Thread.sleep(500);
            run();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println(t1.getState());

    }

    @Override
    public void run() {
        int i = 0;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(
                    "src/datastructures.BitUtils.java"));
            String l;
            String name = Thread.currentThread().getName();

            while ((l = reader.readLine()) != null) {
                System.out.println(name + " " + i++ + " " + l);
                Thread.sleep(500);
            }
            reader.close();
        } catch (Exception e) {
        }

    }
}
