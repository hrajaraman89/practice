package io;

import java.nio.ByteBuffer;

public class ByteBufferTest {

    /**
     * @param args
     */
    public static void main(String[] args) {
        ByteBuffer buf = ByteBuffer.allocate(3);

        buf.put((byte) 1);
        buf.putChar((char) 42);

        buf.flip();
        System.out.println(buf.position() + " " + buf.capacity() + " "
                + buf.remaining() + " " + buf.limit());
        // buf.flip();
        // System.out.println(buf.position() + " " + buf.capacity() + " "
        // + buf.remaining() + " " + buf.limit());

        byte[] buffy = new byte[5];
        int min = Math.min(buffy.length, buf.remaining());
        System.out.println(min);
        buf.get(buffy, 0, min);

        for (byte b : buffy) {
            System.out.println(b);
        }

        buf.clear();

        System.out.println(buf.position() + " " + buf.capacity() + " "
                + buf.remaining() + " " + buf.limit());

    }
}
