package io;

import java.io.OutputStream;
import java.io.PrintWriter;

public class MyPrintWriter extends PrintWriter {

    public MyPrintWriter(OutputStream arg0) {
        super(arg0);
    }

    public void write(int i) {
        System.out.println("Calling write on " + i);
    }

    public void write(String s, int offset, int len) {
        System.out.println("Calling write on s,o,l " + s);
        // super.write(s);
    }

    public void write(char[] buf, int o, int len) {
        System.out.println("calling buf,o,len");
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        PrintWriter writer = new MyPrintWriter(System.out);
        writer.write("This is a temp", 0, 5);
        writer.print("This is a temp print");
        writer.write("Char[]".toCharArray());
        writer.write(1);
        writer.print(2);
    }
}
