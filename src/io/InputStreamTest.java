package io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class InputStreamTest {

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        String path1 = "/Users/Hari/Pictures/random/pic.jpeg";
        File f = new File(path1);
        BufferedReader in = new BufferedReader(new InputStreamReader(
                new FileInputStream(f)));
        String path2 = "/Users/Hari/Desktop/temp.jpeg";
        PrintWriter out = new PrintWriter(new FileOutputStream(new File(path2)));

        int read;
        char[] cbuf = new char[1024];
        int count = 0;

        while ((read = in.read()) != -1) {
            out.write(read);
            ++count;
        }

        in.close();
        out.close();

        System.out.println("done " + count);

        FileComparer.compare(path1, path2);

    }
}
