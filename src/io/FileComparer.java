package io;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class FileComparer {

    public static void main(String[] args) throws IOException {
        String path1 = "/home/cis455/internet-web-systems/HW1/test-files/img1.jpg";
        String path2 = "/home/cis455/Downloads/img1.jpg";

        compare(path1, path2);

    }

    public static void compare(String path1, String path2) throws IOException {
        File f1 = new File(path1);
        File f2 = new File(path2);

        FileInputStream in1 = new FileInputStream(f1);
        FileInputStream in2 = new FileInputStream(f2);

        System.out.println(f1.length());
        System.out.println(f2.length());

        int i1, i2;
        int byteNum = 0;

        while (true) {
            ++byteNum;
            i1 = in1.read();
            i2 = in2.read();

            if (i1 != i2) {
                System.out.printf(
                        "byte %d is off. %s has %s %d and %s has %s %d\n",
                        byteNum, path1, (char) i1, i1, path2, (char) i2, i2);
                break;
            } else {
                if (i1 == -1 || i2 == -1) {
                    System.out.println("Equal!");
                    break;
                }
            }
        }

        in1.close();
        in2.close();
    }
}
