package io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SealedObject;
import javax.crypto.SecretKey;
import javax.crypto.spec.PBEKeySpec;

public class ObjectOutputStreamTest {

    HashMap<String, Object> map;

    public ObjectOutputStreamTest() throws FileNotFoundException, IOException,
            Exception {
        map = new HashMap<String, Object>();

        map.put("key1", "string");
        map.put("key2", 2);
        map.put("key3", new byte[] { 1, 2, 3, 45 });

        String clearText = "simple";
        char[] password = "password".toCharArray();

        PBEKeySpec keySpec = new PBEKeySpec(password);

        String algorithm = "DES";
        SecretKey secretKey = KeyGenerator.getInstance("DES").generateKey();

        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);

        map.put("key4", new SealedObject(clearText, cipher));

        String file = "test";
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(
                file));

        out.writeObject(map);

        ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));

        Map<String, Object> map2 = (Map<String, Object>) in.readObject();

        for (String key : map2.keySet()) {
            System.out.println(key + " " + map2.get(key).toString());
        }

        SealedObject o = (SealedObject) map2.get("key4");

        String plain = (String) o.getObject(secretKey);
        System.out.println(plain);
    }

    public static void main(String... args) throws FileNotFoundException,
            IOException, Exception {
        new ObjectOutputStreamTest();
    }
}
