package interviewstreet;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Hari
 * Date: 8/8/13
 * Time: 8:34 PM
 * To change this template use File | Settings | File Templates.
 */

public class Hanoi {

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String diskThenPegs = br.readLine();
        String[] split = diskThenPegs.split(" ");

        int numDisks = Integer.parseInt(split[0]);
        int numPegs = Integer.parseInt(split[1]);

        System.out.println("Num disks: " + numDisks);
        System.out.println("Num pegs: " + numPegs);

        int[] diskToPeg = new int[numDisks];

        String[] initialConfiguration = br.readLine().split(" ");

        Peg[] pegs = new Peg[numPegs];

        for (int radius = numDisks - 1; radius >= 0; radius--) {
            int pegIndex = Integer.parseInt(initialConfiguration[radius]) - 1;

            System.out.printf("Peg with radius %d goes to peg %d\n", radius, pegIndex);

            diskToPeg[radius] = pegIndex;

            if (pegs[pegIndex] == null) {
                pegs[pegIndex] = new Peg();
            }

            pegs[pegIndex].addDisk(radius);
        }

        int[] targetPegForDisk = new int[numDisks];

        String[] finalConfiguration = br.readLine().split(" ");

        for (int i = numDisks - 1; i >= 0; i--) {
            targetPegForDisk[i] = Integer.parseInt(finalConfiguration[i]) - 1;

            System.out.printf("Disk with radius %d should be in peg %d\n", i, targetPegForDisk[i]);
        }

        List<String> moves = new ArrayList<String>();

        for (int i = numDisks - 1; i >= 0; i--) {
            if (diskToPeg[i] == targetPegForDisk[i]) continue;
            else {
                int from = diskToPeg[i];
                for (int j = 0; j < pegs.length; j++) {
                    if (pegs[j].canAdd(i)) {
                        int to = j;
                        pegs[j].addDisk(i);
                        moves.add(from + " " + to);
                    }
                }
            }
        }

        for (String s : moves){
            System.out.println(s);
        }
    }
}

class Peg {
    List<Integer> disks = new ArrayList<Integer>();

    public void addDisk(Integer radius) throws Exception {
        if (!canAdd(radius)) {
            throw new Exception("violation!");
        }

        disks.add(radius);
    }

    public boolean canAdd(Integer radius) {
        return disks.isEmpty() || (disks.get(disks.size()-1) > radius);
    }
}



