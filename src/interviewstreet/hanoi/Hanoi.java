package interviewstreet.hanoi;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Hari
 * Date: 8/10/13
 * Time: 3:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class Hanoi {
    private Peg[] allPegs;
    private int[] diskRadiusToPegIndex;
    private List<String> moves = new ArrayList<String>();

    private void addDisksToPegs(int[] disks) {
        diskRadiusToPegIndex = new int[disks.length + 1]; //radii are not 0 based, so we'll add an extra spot.

        for (int i = disks.length - 1; i >= 0; i--) {
            int pegIndex = disks[i];

            allPegs[pegIndex].add(i);
            System.out.printf("Disks with radius %d goes to peg %d\n", i, pegIndex);

            diskRadiusToPegIndex[i] = pegIndex;
        }
    }

    private void makeMoves(int[] finalConfiguration) {
        for (int i = finalConfiguration.length - 1; i >= 0; i--) {
            Peg peg = allPegs[diskRadiusToPegIndex[i]];
            makeMove(peg, i, allPegs[finalConfiguration[i]]);

        }
    }

    private void makeMove(Peg peg, int radius, Peg target) {

        while (!peg.canPop(radius)) {
            freeUp(peg, target);
        }

        while (!target.canAdd(radius)) {
            freeUp(target, peg);
        }
    }

    private void freeUp(Peg peg, Peg notOntoPeg) {
        int currentMinDiff = Integer.MAX_VALUE;
        Peg currentTarget = null;

        int blockingDiskRadius = peg.peek();

        for (int i = 1; i < allPegs.length; i++) {
            Peg p = allPegs[i];

            if (p == notOntoPeg) {
                continue;
            } else {
                if (p.canAdd(blockingDiskRadius)) {
                    int pPeek = p.peek();

                    if (pPeek - blockingDiskRadius < currentMinDiff) {
                        currentMinDiff = pPeek - blockingDiskRadius;
                        currentTarget = p;
                    }
                }
            }
        }

        currentTarget.add(peg.pop());
        recordMove(peg, currentTarget);
    }

    private void recordMove(Peg from, Peg to) {
        this.moves.add(from.index() + " " + to.index());
    }


}
