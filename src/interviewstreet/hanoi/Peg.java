package interviewstreet.hanoi;

import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * User: Hari
 * Date: 8/10/13
 * Time: 3:00 PM
 * To change this template use File | Settings | File Templates.
 */

public class Peg {
    private Stack<Integer> peg;
    private int index;

    public Peg(int index) {
        this.peg = new Stack<Integer>();
        this.index = index;
    }

    public boolean canAdd(int radius) {
        return peg.isEmpty() || peg.peek() > radius;
    }

    public void add(int radius) {
        if (canAdd(radius)) {
            peg.push(radius);
        }
    }

    public boolean canPop(int radius) {
        return peg.peek() == radius;
    }

    public int pop() {
        return peg.pop();
    }

    public int peek() {
        return peg.peek();
    }

    public int index() {
        return index;
    }
}
