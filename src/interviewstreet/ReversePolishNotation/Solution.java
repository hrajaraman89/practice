package interviewstreet.ReversePolishNotation;

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //stdin from faq

        args = new String[]{"*xx", "xxx***", "**xx"};


        for (int i = 0; i < args.length; i++) {
            outputMinimumOperations(args[i]);
        }
    }

    private static void outputMinimumOperations(String rpn) {
        String originalRpn = rpn;

        int indexOfValidOperations;

        while ((indexOfValidOperations = rpn.indexOf("xx*")) > -1) {

            //TODO: creating new strings is BAD
            rpn = rpn.replace("xx*", "x");
        }

        if (rpn.length() == 0 || "x".equals(rpn)) {
            System.out.println("0");
            return;
        }

        int countX = count(originalRpn, 'x');
        int countStars = count(originalRpn, '*');
        
        /*

        *xx
        for every * before the final x.. remove it. update count stars
        now add * so that there are countX - 1 stars at the end

        */

        int lastX = originalRpn.lastIndexOf("x");

        int lastFoundStar = 0;

        int moves = 0;

        int starIndex;

        while ((starIndex = originalRpn.indexOf("*", lastFoundStar)) < lastX && starIndex != -1) {

            if (checkChar(originalRpn, starIndex - 1, 'x') && checkChar(originalRpn, starIndex - 2, 'x')) {
                lastFoundStar = starIndex + 1;
                System.out.println(lastFoundStar);
                continue;
            }
            originalRpn = originalRpn.replaceFirst("\\*", "");
            ++moves;

            --countStars;
        }


        if (countX > countStars) {
            moves += countX - countStars - 1;
        } else {
            moves += countStars - countX + 1;
        }

        System.out.println(moves);
    }

    private static boolean checkChar(String str, int index, char c) {
        return index < str.length() - 1 ? false : str.charAt(index) == c;
    }

    private static int count(String str, char c) {
        int count = 0;

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == c) {
                ++count;
            }
        }

        return count;
    }

}


        
        