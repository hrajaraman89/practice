package interviewstreet.ReversePolishNotation;/* Enter your code here. Read input from STDIN. Print output to STDOUT */

import java.io.*;
import java.util.*;

public class Solution2 {
    public static void main(String[] args) throws Exception {
        //stdin from faq

        args = new String[]{"x**"};


        for (int i = 0; i < args.length; i++) {
            outputMinimumOperations(args[i]);
        }
    }

    private static void outputMinimumOperations(String rpn) {
        boolean keepGoing = true;
        int moves = 0;
        String originalRpn = rpn;

        while (keepGoing) {

            while (rpn.indexOf("xx*") > -1) {

                //TODO: creating new strings is BAD
                rpn = rpn.replace("xx*", "x");
            }

            keepGoing = false;

            if (rpn.length() == 0 || "x".equals(rpn)) {
                System.out.println(moves);
                return;
            }

            originalRpn = rpn;

            while (rpn.indexOf("xxx") > -1) {
                ++moves;
                rpn = rpn.replaceFirst("xxx", "xx*");
                keepGoing = true;
            }

            while (rpn.indexOf("x**") > -1) {
                ++moves;
                rpn = rpn.replaceFirst("x\\*\\*", "xx*");
                keepGoing = true;
            }
        }
        /*

        *xx
        for every * before the final x.. remove it. update count stars
        now add * so that there are countX - 1 stars at the end

        */


        int countX = count(originalRpn, 'x');
        int countStars = count(originalRpn, '*');
        int lastFoundStar = 0;
        int lastX = originalRpn.lastIndexOf("x");
        int starIndex;

        while ((starIndex = originalRpn.indexOf("*", lastFoundStar)) < lastX && starIndex != -1) {

            if (checkChar(originalRpn, starIndex - 1, 'x') && checkChar(originalRpn, starIndex - 2, 'x')) {
                lastFoundStar = starIndex + 1;
                continue;
            }
            originalRpn = originalRpn.replaceFirst("\\*", "");
            ++moves;

            --countStars;
        }

        int movesToMake = countX - countStars - 1;

        if (countX > countStars) {

            moves += movesToMake;
        } else {
            moves -= movesToMake;
        }


        System.out.println(moves);
    }

    private static boolean checkChar(String str, int index, char c) {
        return index < str.length() - 1 ? false : str.charAt(index) == c;
    }

    private static int count(String str, char c) {
        int count = 0;

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == c) {
                ++count;
            }
        }

        return count;
    }
}

