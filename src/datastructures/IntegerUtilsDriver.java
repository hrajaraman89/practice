package datastructures;

public class IntegerUtilsDriver {

    public static void main(String[] args) {
        int[] sortedArray = new int[] { 1, 1, 2, 5, 5, 5, 5, 5, 5, 6, 7, 8, 9,
                10 };

        int occurrences = IntegerUtils.findOccurrencesSorted(sortedArray, 11);
        // System.out.println(occurrences);

        int[] op1 = new int[] { 0, 1, 2, 5 };
        int[] op2 = new int[] { 1, 7, 5, 0 };

        int[] answer = IntegerUtils.add(op1, op2);

        // printArray(answer);

        // int x = datastructures.IntegerUtils.findMedian(1, 2, 3);
        // System.out.println(x);
        // x = datastructures.IntegerUtils.findMedian(1, 3, 2);
        // System.out.println(x);
        // x = datastructures.IntegerUtils.findMedian(3, 1, 2);
        // System.out.println(x);
        // x = datastructures.IntegerUtils.findMedian(2, 3, 1);
        // System.out.println(x);

        LinkedList<Integer> list1 = new LinkedList<Integer>();
        list1.add(5);
        list1.add(2);
        Node<Integer> node1 = list1.head;

        LinkedList<Integer> list2 = new LinkedList<Integer>();
        list2.add(6);
        list2.add(2);
        Node<Integer> node2 = list2.head;

        Node<Integer> sumNode = IntegerUtils.add(node1, node2);
        LinkedList<Integer> list = new LinkedList<Integer>();
        list.head = sumNode;

        list.print();

    }

    static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + ", ");
        }

        System.out.println();
    }

}
