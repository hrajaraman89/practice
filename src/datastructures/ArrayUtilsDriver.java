package datastructures;

public class ArrayUtilsDriver {

    /**
     * @param args
     */
    public static void main(String[] args) {

        int dimension = 3;

        Integer[][] array = new Integer[dimension][dimension];
        int count = 1;

        for (int i = 0; i < dimension; i++)
            for (int j = 0; j < dimension; j++)
                array[i][j] = count++;

        // datastructures.ArrayUtils.print2DArray(array);

        // datastructures.ArrayUtils.printZigZag(3);
        ArrayUtils.printZigZag(5);

        // System.out.println(Integer.parseInt("aC198"));
        // System.out.println(Integer.parseInt(""));
        System.out.println(Integer.parseInt(null));

    }

}
