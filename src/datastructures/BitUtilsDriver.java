package datastructures;

public class BitUtilsDriver {

    public static void main(String[] args) {

        int i = 8;

        String s = BitUtils.intToBinaryString(i);
        System.out.println(s);

        i = BitUtils.binaryStringToInt(s);
        System.out.println(i);

        String n = "10000000000";
        String m = "10101";
        i = 2;
        int j = 6;

        String answer = BitUtils.makeSubString(n, m, i, j);
        System.out.println(answer);

    }
}
