package datastructures;

public class IntegerUtils {

    public static int findOccurrencesSorted(int[] array, int target) {
        return findOccurrencesSorted(array, target, 0, array.length - 1);
    }

    private static int findOccurrencesSorted(int[] array, int target,
            int startIndex, int endIndex) {
        if (endIndex < startIndex)
            return 0;

        else {
            int mid = (startIndex + endIndex) / 2;

            int finalCount = 0;

            if (array[mid] == target) {
                finalCount = 1
                        + findOccurrencesSorted(array, target, startIndex,
                                mid - 1)
                        + findOccurrencesSorted(array, target, mid + 1,
                                endIndex);
            } else if (array[mid] < target) {
                finalCount = findOccurrencesSorted(array, target, mid + 1,
                        endIndex);
            } else {
                finalCount = findOccurrencesSorted(array, target, startIndex,
                        mid - 1);
            }

            return finalCount;

        }
    }

    public static int[] add(int[] operand1, int[] operand2) {
        int[] ret = new int[Math.max(operand1.length, operand2.length) + 1];

        int operand1Ptr = operand1.length - 1;
        int operand2Ptr = operand2.length - 1;
        int answerPtr = ret.length - 1;

        int maxElement = 10;
        int currentElement = 0;
        boolean carryover = false;

        while (operand1Ptr >= 0 && operand2Ptr >= 0) {
            currentElement = operand1[operand1Ptr] + operand2[operand2Ptr];
            if (carryover) {
                currentElement++;
            }

            carryover = currentElement >= maxElement;
            currentElement = currentElement % maxElement;
            ret[answerPtr] = currentElement;

            --answerPtr;
            --operand1Ptr;
            --operand2Ptr;
        }

        int[] remainingArray;
        int remainingArrayPtr;

        if (operand1Ptr > operand2Ptr) {
            remainingArray = operand1;
            remainingArrayPtr = operand1Ptr;
        } else {
            remainingArray = operand2;
            remainingArrayPtr = operand2Ptr;
        }

        while (remainingArrayPtr >= 0) {
            ret[answerPtr] = remainingArray[remainingArrayPtr];

            --answerPtr;
            --remainingArrayPtr;
        }

        if (ret[0] == 0) {
            int leadingNonZeroIndex = 0;

            while (ret[leadingNonZeroIndex] == 0) {
                ++leadingNonZeroIndex;
            }

            int[] retCopy = new int[ret.length - leadingNonZeroIndex];
            System.arraycopy(ret, leadingNonZeroIndex, retCopy, 0, ret.length
                    - leadingNonZeroIndex);
            ret = retCopy;
        }

        return ret;
    }

    public static int findMedian(int a, int b, int c) {
        if (a < b) {
            if (b < c)
                return b;
            else {
                return Math.max(c, a);
            }
        } else {
            if (b > c)
                return b;
            else {
                return Math.min(c, a);
            }
        }

    }

    public static Node<Integer> add(Node<Integer> head1, Node<Integer> head2) {
        Node<Integer> ptr1 = head1;
        Node<Integer> ptr2 = head2;
        Node<Integer> answerHead = null;
        boolean carryover = false;
        Node<Integer> temp = null;

        while (ptr1 != null && ptr2 != null) {
            temp = new Node<Integer>(ptr1.value + ptr2.value);
            if (carryover)
                temp.value = temp.value + 1;

            carryover = temp.value >= 10;
            temp.value = temp.value % 10;

            if (answerHead == null) {
                answerHead = temp;
            } else {
                temp.next = answerHead;
                answerHead = temp;
            }

            ptr1 = ptr1.next;
            ptr2 = ptr2.next;
        }

        if (ptr1 == null && ptr2 == null) {
            if (carryover) {
                answerHead.value = answerHead.value + 10;
            }
        } else {
            Node<Integer> remainingNodes = ptr1 == null ? ptr2 : ptr1;

            while (remainingNodes != null) {
                temp = new Node<Integer>(remainingNodes.value);
                if (carryover) {
                    temp.value = temp.value + 1;
                    carryover = false;
                }
                temp.next = answerHead;
                answerHead = temp;

                remainingNodes = remainingNodes.next;
            }
        }

        return answerHead;
    }
}
