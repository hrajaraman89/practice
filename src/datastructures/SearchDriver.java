package datastructures;

public class SearchDriver {
    public static void main(String[] args) {
        int[] array = new int[] { 0, 1, 2, 3, 4, 5, 6, 7 };
        boolean found = Search.binarySearch(array, 6);
        System.out.println(found);

        found = Search.binarySearch(array, 5);
        System.out.println(found);
    }
}
