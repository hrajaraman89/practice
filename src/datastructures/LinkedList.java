package datastructures;

public class LinkedList<T> {

    Node<T> head;

    public LinkedList() {
        head = null;
    }

    void add(T value) {
        Node<T> node = new Node<T>(value);

        if (head == null) {
            head = node;
        } else {
            Node<T> ptr = head;
            while (ptr.next != null) {
                ptr = ptr.next;
            }

            ptr.next = node;
        }
    }

    void removeIndex(int index) {
        if (index == 0) {
            removeFirst();
        } else {
            // stop at node before
            index--;
            int currentIndex = 0;
            Node<T> before = head;
            while (currentIndex < index) {
                before = before.next;
                if (before == null) {
                    return;
                }
                currentIndex++;
            }

            before.next = before.next.next;
        }
    }

    T removeFirst() {
        T retValue = null;
        if (head != null) {
            retValue = head.value;
            head = head.next;
        }

        return retValue;
    }

    T valueAtIndex(int index) {
        int currentIndex = 0;
        Node<T> ptr = head;

        while (currentIndex < index) {
            ptr = ptr.next;
        }

        return ptr.value;
    }

    void addAfterIndex(int index, T nextVal) {
        Node<T> ptr = head;
        int currentIndex = 0;
        while (currentIndex < index) {
            ptr = ptr.next;
            currentIndex++;
        }

        Node<T> nodeToAdd = new Node<T>(nextVal);
        nodeToAdd.next = ptr.next;
        ptr.next = nodeToAdd;
    }

    void addFirst(T nextVal) {
        Node<T> node = new Node<T>(nextVal);

        node.next = head;
        head = node;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        Node<T> ptrNode = head;

        while (ptrNode != null) {
            builder.append(ptrNode.value);
            if (ptrNode.next != null) {
                builder.append(" => ");
                ptrNode = ptrNode.next;
            } else {
                builder.append("\n");
                break;
            }
        }

        return builder.toString();
    }

    void print() {
        System.out.print(this.toString());
    }

}
