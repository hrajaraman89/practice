package datastructures;

public class LinkedListDriver {
    private static int currentCount;

    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<Integer>();

        list.add(getNextVal());
        list.print();

        list.add(getNextVal());
        list.print();
        list.add(getNextVal());
        list.print();

        list.addFirst(getNextVal());
        list.print();

        list.addAfterIndex(2, getNextVal());
        list.print();

        list.removeIndex(4);
        list.print();
        list.removeIndex(0);
        list.print();
        list.addAfterIndex(1, getNextVal());
        list.print();
    }

    private static Integer getNextVal() {
        return ++currentCount;
    }
}
