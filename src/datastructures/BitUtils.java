package datastructures;

public class BitUtils {

    static String makeSubString(String ns, String ms, int i, int j) {
        int n = BitUtils.binaryStringToInt(ns);
        int m = BitUtils.binaryStringToInt(ms);
        n = n << j;
        m = m << i;

        return BitUtils.intToBinaryString(n + m);
    }

    static String intToBinaryString(int i) {
        String string = "";

        while (i != 0) {
            string = i % 2 + string;
            i = i / 2;
        }

        return string;
    }

    static int binaryStringToInt(String s) {
        int power = s.length() - 1;
        int answer = 0;
        int index = 0;
        int bit;

        while (index < s.length()) {
            bit = s.charAt(index) - '0';
            answer += Math.pow(2, power) * bit;
            index++;
            power--;
        }

        return answer;

    }

}
