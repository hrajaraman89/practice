package datastructures;

public class Search {

    public static boolean binarySearch(int[] array, int target) {
        int upper = array.length - 1;
        int lower = 0;
        return binarySearch(array, lower, upper, target);

    }

    private static boolean binarySearch(int[] array, int lower, int upper,
            int target) {

        if (upper < lower)
            return false;

        int mid = lower + (upper - lower) / 2;

        if (array[mid] - target == 0) {
            return true;
        } else if (array[mid] - target < 0) {
            return binarySearch(array, mid + 1, upper, target);
        } else {
            return binarySearch(array, lower, mid - 1, target);
        }

    }
}
