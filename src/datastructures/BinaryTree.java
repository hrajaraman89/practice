package datastructures;

import java.util.HashSet;

public class BinaryTree<T> {
    T value;
    BinaryTree<T> left;
    BinaryTree<T> right;
    BinaryTree<T> parent;

    public BinaryTree(T val) {
        this.value = val;
    }

    public void preorderTraverse() {
        this.print();
        if (this.left != null) {
            this.left.preorderTraverse();
        }
        if (this.right != null) {
            this.right.preorderTraverse();
        }
    }

    public void iterativePreorderTraverse() {
        Stack<BinaryTree<T>> stack = new Stack<BinaryTree<T>>();
        stack.push(this);
        BinaryTree<T> current;

        while (stack.peek() != null) {
            current = stack.pop();
            current.print();

            if (current.right != null)
                stack.push(current.right);

            if (current.left != null)
                stack.push(current.left);
        }

    }

    public void print() {
        System.out.println(value);
    }

    public void addLeft(T value) {
        if (this.left == null) {
            this.left = new BinaryTree<T>(value);
            this.left.parent = this;
        }

    }

    public void addRight(T value) {
        if (this.right == null) {
            this.right = new BinaryTree<T>(value);
            this.right.parent = this;
        }

    }

    public String toString() {
        return String.format("%d,%s,%s\n", this.value,
                (this.left != null ? this.left.value.toString() : null),
                (this.right != null ? this.right.value.toString() : null));
    }

    public static <T> BinaryTree<T> findCommonAncestor(BinaryTree<T> tree1,
            BinaryTree<T> tree2) {

        HashSet<BinaryTree<T>> tree1Parents = new HashSet<BinaryTree<T>>();

        while (tree1 != null) {
            tree1Parents.add(tree1);
            tree1 = tree1.parent;
        }

        while (tree2 != null) {
            if (tree1Parents.contains(tree2)) {
                return tree2;
            } else {
                tree2 = tree2.parent;
            }
        }

        return null;
    }

    public static <T> BinaryTree<T> findSmallestElement(BinaryTree<T> root,
            int n) {
        return null;
    }
}
