package datastructures;

import java.util.HashMap;
import java.util.HashSet;

public class StringUtils {

    private static int counter;

    public static int atoi(String str) {
        int i = 0;
        int pow = 0;
        int zeroChar = (int) '0';

        int currentIndex = str.length() - 1;

        while (currentIndex >= 0) {
            char curChar = str.charAt(currentIndex);
            if (curChar == '-') {
                return -i;
            } else {
                int currentChar = (int) curChar;
                i += (currentChar - zeroChar) * Math.pow(10, pow);
                pow++;
                --currentIndex;
            }
        }

        return i;
    }

    public static String itoa(int i) {
        StringBuilder builder = new StringBuilder();
        while (i != 0) {
            char lastDigit = (char) (i % 10 + (int) '0');
            builder.insert(0, lastDigit);
            i = i / 10;
        }

        return builder.toString();

    }

    public static String removeCharsFromString(String input,
            char[] charsToRemove) {
        HashSet<Character> charsToRemoveSet = new HashSet<Character>();

        for (int i = 0; i < charsToRemove.length; ++i) {
            charsToRemoveSet.add(charsToRemove[i]);
        }

        char[] inputCharArray = input.toCharArray();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < inputCharArray.length; ++i) {
            if (!charsToRemoveSet.contains(inputCharArray[i])) {
                builder.append(inputCharArray[i]);
            }
        }

        return builder.toString();
    }

    public static Character firstNonRepeatingChar(String input) {
        HashMap<Character, Integer> map = new HashMap<Character, Integer>();

        for (int i = 0; i < input.length(); i++) {
            Integer value = 1;
            if (map.containsKey(input.charAt(i))) {
                value = map.get(input.charAt(i));
                value++;
            }
            map.put(input.charAt(i), value);
        }

        for (int i = 0; i < input.length(); i++) {
            if (map.get(input.charAt(i)) == 1) {
                return input.charAt(i);
            }
        }

        return null;
    }

    public static void permute(String str) {
        counter = 0;
        permute(str, "");
    }

    private static void permute(String str, String currentString) {
        if ("".equals(str)) {
            System.out.println(++counter + " " + currentString);
            return;
        }

        for (int i = 0; i < str.length(); i++) {
            String prevString = str;
            String prevCurString = currentString;

            currentString += str.charAt(i);
            str = str.substring(0, i) + str.substring(i + 1);

            permute(str, currentString);

            str = prevString;
            currentString = prevCurString;
        }
    }

    public static void subset(String str) {
        int max = (int) Math.pow(2, str.length()) - 1;
        int current = 0;

        while (current <= max) {
            String subset = "";
            String binary = Integer.toBinaryString(current);
            while (binary.length() < str.length()) {
                binary = '0' + binary;
            }
            for (int i = 0; i < binary.length(); i++) {
                if (binary.charAt(i) == '1') {
                    subset += str.charAt(i);
                }
            }
            System.out.println(subset);
            current++;
        }
    }
}
