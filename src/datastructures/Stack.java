package datastructures;

public class Stack<T> {
    LinkedList<T> list;
    private int size = 0;

    public Stack() {
        list = new LinkedList<T>();
    }

    public void push(T value) {
        ++size;
        list.addFirst(value);
    }

    public T pop() {
        --size;
        return list.removeFirst();
    }

    public T peek() {
        if (size == 0)
            return null;
        return list.valueAtIndex(0);
    }

    public String toString() {
        return list.toString();
    }

    public void print() {
        list.print();
    }

}
