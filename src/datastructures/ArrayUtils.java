package datastructures;

public class ArrayUtils {

    private static int maxIndex;

    public static <T> void print2DArray(T[][] array) {
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array.length; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.println();
        }
    }

    public static void printZigZag(int dimension) {
        Integer[][] array = new Integer[dimension][dimension];
        int count = 1;
        int maxCount = dimension * dimension;
        maxIndex = dimension - 1;

        int currentRow = 0;
        int currentColumn = 0;

        while (count <= maxCount) {
            // print2DArray(array);
            // System.out.println();

            while (isWithinBounds(currentRow) && isWithinBounds(currentColumn)) {

                array[currentRow][currentColumn] = count;
                count++;

                // try down-left
                if (isSafeDownLeft(currentRow, currentColumn)) {
                    ++currentRow;
                    --currentColumn;
                } else {
                    if (isSafeDown(currentRow, currentColumn)) {
                        ++currentRow;
                    } else {
                        // can only go right
                        ++currentColumn;
                    }
                    break;
                }

            }

            while (isWithinBounds(currentRow) && isWithinBounds(currentColumn)) {
                array[currentRow][currentColumn] = count;
                count++;

                if (isSafeUpRight(currentRow, currentColumn)) {
                    --currentRow;
                    ++currentColumn;
                } else {
                    if (isSafeRight(currentRow, currentColumn)) {
                        ++currentColumn;
                    } else {
                        ++currentRow;
                    }

                    break;
                }
            }
        }

        print2DArray(array);

    }

    private static boolean isSafeRight(int currentRow, int currentColumn) {
        return isWithinBounds(currentRow) && isWithinBounds(currentColumn + 1);
    }

    private static boolean isSafeUpRight(int currentRow, int currentColumn) {
        return isWithinBounds(currentRow - 1)
                && isWithinBounds(currentColumn + 1);
    }

    private static boolean isSafeDown(int currentRow, int currentColumn) {
        return isWithinBounds(currentRow + 1) && isWithinBounds(currentColumn);
    }

    private static boolean isSafeDownLeft(int currentRow, int currentColumn) {
        return isWithinBounds(currentRow + 1)
                && isWithinBounds(currentColumn - 1);
    }

    private static boolean isWithinBounds(int currentIndex) {
        return currentIndex >= 0 && currentIndex <= maxIndex;
    }
}
