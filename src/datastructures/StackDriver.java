package datastructures;

public class StackDriver {
    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<Integer>();
        stack.print();

        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.print();

        stack.pop();
        stack.print();
        stack.pop();
        stack.print();

        Integer peek = stack.peek();

        System.out.println(peek);
    }
}
