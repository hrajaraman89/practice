package datastructures;

public class BinaryTreeDriver {
    private static int count;
    private static int desiredDepth;

    public static void main(String[] args) {
        BinaryTree<Integer> root = new BinaryTree<Integer>(count++);
        desiredDepth = 2;
        addElementsToTree(root, 0);

        // root.preorderTraverse();
        // System.out.println("\n");
        // root.iterativePreorderTraverse();

        BinaryTree<Integer> tree1 = root.left.left;
        BinaryTree<Integer> tree2 = root.left;

        BinaryTree<Integer> ancestor = BinaryTree.findCommonAncestor(tree1,
                tree2);

        System.out.println(ancestor.value);
    }

    private static void addElementsToTree(BinaryTree<Integer> root,
            int currentLevel) {
        if (desiredDepth == currentLevel)
            return;

        root.addLeft(count++);
        root.addRight(count++);

        addElementsToTree(root.left, currentLevel + 1);
        addElementsToTree(root.right, currentLevel + 1);
    }

}
